# audio-recorder-functional-tests

#### Endpoint Gravador:

- Para que o gravador possa realizar upload/downlaod de áudios, é necessário a criação de um server para receber as requisições. na pasta docker do projeto, há um server Node.js para ser inicializado em Docker. Após a inicialização, basta informar a url na página de execução do gravador para o serviço rode corretamente.
- Para mais informações acesse o arquivo "readme" na pasta docker > endpoint. 
 
#### Prerequisites:

  - [JDK 8](https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
  - [Maven](https://maven.apache.org/download.cgi)
  
#### Optional but recommended:

  - [Sikuli IDE](http://sikulix.com) - just for image capture

##### Structure Basic Overview:

`src/test/java/tests` : where to keep the test runners

`src/test/resources/features` : where to keep the cucumber features

`src/test/java/steps` : where to keep the cucumber steps

`src/test/java/pages` : where to keep the pageobjects

`target/evidencias` : where evidence is generated

`src/test/resources/sikuli-images/[your_sikuli_project].sikuli` : where to keep the sikuli images as sikuli project (folder with extension .sikuli)

_Dependencies_:
*   java
*   cucumber
*   Extentreports
*   junit
*   sikulix

##### Runnig Tests with maven:
```java
mvn test
```
or

```java
mvn test -Dtest=NameOfClassTest
```

##### Referencing images:

```java
@FindBy(image = "menu.png")
private SikuliElement menu;
```

Or increasing similarity:

```java
@FindBy(image = "menu.png", similarity = 85)
private SikuliElement menu;
```

Or with more images and coordinates:

```java
@FindBy(images = {"menu-windows10.png", "menu-windows8.png"}, similarity = 85, x = 10, y = 25)
private SikuliElement menu;
```

##### Examples of some useful sikuli commands:

`menu.wait();`

`menu.wait(30);`

`menu.click();`

`menu.exists();`

`menu.exists(30);`

`menu.type("text to type on component");`

`menu.type("c", Key.CTRL)`

`menu.waitVanish();`

`menu.waitVanish(30);`

_More commands on_ [sikulix documentation](https://sikulix-2014.readthedocs.io/en/latest)

##### Recommended practices to automate with Sikuli:
* Capture the smallest part of the image as possible. If the image involves multiple elements, the test may be sensitive to changes.
* Run tests on VM or Docker Container setting especific resolution like 1366x768.
* Use as few images as possible. Use the shortcuts in the application to interact with the components.
* Images captured at higher resolutions (1920x1080) usually work at lower resolutions (1366x768), the opposite does not.
