function Controller()
{
    gui.clickButton(buttons.NextButton);
	installer.installationFinished.connect(function(){gui.clickButton(buttons.NextButton);});
}

Controller.prototype.DynamicSYSTEMCallback = function ()
{
	gui.clickButton(buttons.NextButton);
}

Controller.prototype.ReadyForInstallationPageCallback = function ()
{
	gui.clickButton(buttons.NextButton);
}

Controller.prototype.PerformInstallationPageCallback = function ()
{
	gui.clickButton(buttons.NextButton);
}

Controller.prototype.FinishedPageCallback = function ()
{
	gui.clickButton(buttons.FinishButton );
}