#language: pt
#encoding: utf-8
@chrome
Funcionalidade: Idiomas

	@chrome
  Cenario: Idioma Espanhol
    Dado que abro o gravador
    Entao deve ser exibido o campo para informar o servidor

  Cenario: Idioma Portugues
    Dado que abro o study "1.3.46.670589.11.4140.6.123890950282789940"
    Quando clico para alterar o idioma para portugues
    Entao as descricoes do sistema devem ser alteradas para portugues
