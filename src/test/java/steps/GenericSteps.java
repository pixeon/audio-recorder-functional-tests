package steps;

import java.io.IOException;
import org.junit.Assert;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pages.GenericPage;

public class GenericSteps {
	
	@Entao("^deve apresentar a descricao \"([^\"]*)\"$")
	public void deveApresentarDescricao(String descricao) {
		GenericPage genericPage = new GenericPage();
		Assert.assertTrue(genericPage.deveApresentarDescricao(descricao));
	}
	
	@Entao("^a janela \"([^\"]*)\" nao deve estar visivel$")
	public void elementoNaoDeveEstarVisivel(String id) {
		GenericPage genericPage = new GenericPage();
		Assert.assertTrue(genericPage.elementoNaoDeveEstarVisivel(id));
	}

	@Entao("^o elemento \"([^\"]*)\" deve apresentar a imagem \"([^\"]*)\"$")
	public void deveApresentarImagem(String id, String imagem) throws IOException {
		GenericPage genericPage = new GenericPage();
		Assert.assertTrue(genericPage.elementodeveApresentarImagem(id, imagem));
	}
	
	@Entao("^o elemento com id \"([^\"]*)\" deve estar visivel$")
	public void elementoDeveEstarVisivel(String id) {
		GenericPage genericPage = new GenericPage();
		Assert.assertTrue(genericPage.elementoDeveEstarVisivel(id));
	}
	
	@Quando("^maximizo o navegador$")
	public void maximizarNavegador() {
		GenericPage genericPage = new GenericPage();
		genericPage.maximizarTela();
	}
	
	@E("^seleciono o painel \"([^\"]*)\"$")
	public void selecionarPainel(int canvas) {
		GenericPage genericPage = new GenericPage();
		genericPage.selecionarPainel("canvas_" + canvas);
	}

	@E("^posiciono na imagem (.*)$")
	public void posicionarNaImagem(int imagem) {
		GenericPage genericPage = new GenericPage();
		genericPage.posicionarNaImagem(imagem);
	}
	
}

