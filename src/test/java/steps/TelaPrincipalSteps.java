package steps;

import org.junit.Assert;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import pages.TelaPrincipalPage;

public class TelaPrincipalSteps {

	@Entao("^deve ser exibido o campo para informar o servidor$")
	public void apresentouCampoServidor() {
		TelaPrincipalPage telaPrincipalPage = new TelaPrincipalPage();
		Assert.assertTrue(telaPrincipalPage.apresentouCampoServidor());
	}

	@Dado("que abro o gravador")
	public void abrirGravador() {
		TelaPrincipalPage telaPrincipalPage = new TelaPrincipalPage();
		telaPrincipalPage.abrirGravador();
	}

}
