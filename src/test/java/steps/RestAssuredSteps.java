package steps;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.hasItem;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.output.WriterOutputStream;
import org.hamcrest.Matchers;
import org.junit.Assert;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.config.LogConfig;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import pages.BasePage;
import pages.RestAssuredPage;
import util.TestRule;
import util.Utils;

public class RestAssuredSteps extends BasePage {

	RequestSpecification request;
	Response response;
	String SESSIONID;
	StringWriter writer;

	private RequestSpecification givenBaseUri(String url) {
		request = given().baseUri(url); // .config(RestAssured.config().decoderConfig(DecoderConfig.decoderConfig().defaultContentCharset("UTF-8")));
		writer = new StringWriter();
		PrintStream captor = new PrintStream(new WriterOutputStream(writer), true);
		request.config(RestAssured.config().logConfig(new LogConfig(captor, true)));
		return request;
	}
	
	@Given("url \"(.*)\"")
	public void url(String url) {
		request = givenBaseUri(url);
	}

	@Given("url in env (.*)")
	public void urlByEnv(String env) {
		request = givenBaseUri(System.getProperty(env, "localhost"));
	}

	@Given("url in propertie (.*)")
	public void urlByProp(String prop) {
		request = givenBaseUri(Utils.getTestProperty(prop));
	}

	@And("path \"(.*)\"")
	public void path(String path) {
		request = request.basePath(path);
	}

	@And("header ([^\\s]+) = (.+)")
	public void headerParam(String field, String param) {
		request = request.header(field, param);
	}

	@And("^query ([^\\s]+) = (.+)")
	public void queryParam(String field, String param) {
		request = request.queryParam(field, param.replaceAll("\"", "").replaceAll(" ", "%20")).urlEncodingEnabled(false);
	}

	@And("form \"(.*)\" value \"(.*)\"")
	public void formParam(String field, String param) {
		request = request.formParam(field, param);
	}

	@And("body")
	public void body(String body) {
		request = request.body(body);
	}

	private RequestSpecification when() {
		return request.when().log().all(true);
	}

	@When("method POST")
	public void post() {
		response = when().post();
	}

	@When("method GET")
	public void get() {
		response = when().get();
		response.then().log().all(true);
		logInfo("<pre>" + writer.toString() + "</pre>");
		System.out.println(writer.toString());
	}
	
	private ValidatableResponse then() {
		ValidatableResponse validatableResponse = response.then();
		return validatableResponse;
	}

	@Then("^status (\\d+)")
	public void status(int status) {
		then().assertThat().statusCode(status);
	}

	@Then("match response \"(.*)\" = \"(.*)\"")
	public void matchResponse(String fieldPath, String param) {
		then().assertThat().body(fieldPath, Matchers.equalTo(param));
	}
	
	@Then("match response \"(.*)\" = (null)")
	public void matchResponseNull(String fieldPath, String param) {
		then().assertThat().body(fieldPath, Matchers.nullValue());
	}
	
	@Then("match response \"(.*)\" = (^\\d+$)")
	public void matchResponseInt(String fieldPath, int param) {
		then().assertThat().body(fieldPath, Matchers.equalTo(param));
	}
	
	@Then("match response \"(.*)\" = ([+-]?\\d*\\.?\\d+)")
	public void matchResponseDouble(String fieldPath, String param) {
		String result = then().extract().body().jsonPath().getString(fieldPath);
		Assert.assertTrue("Expected: " + param + " Actual: " + result, result.equals(param));
	}

	@Then("match response \"(.*)\" contains item \"(.*)\"")
	public void matchResponseContainsItem(String fieldPath, String param) {
		then().assertThat().body(fieldPath, hasItem(param));
	}

	@Then("match response \"(.*)\" contains only item \"(.*)\"")
	public void matchResponseContainsOnlyItem(String fieldPath, String param) {
		then().assertThat().body(fieldPath, everyItem(equalTo(param)));
	}

	@Then("match response \"(.*)\" contains \"(.*)\"")
	public void matchResponseContains(String fieldPath, String param) {
		then().assertThat().body(fieldPath, Matchers.containsString(param));
	}

	@Then("match header \"(.*)\" = \"(.*)\"")
	public void matchHeader(String fieldPath, String param) {
		then().assertThat().header(fieldPath, Matchers.equalTo(param));
	}

	@Then("match response to schema \"(.*)\"")
	public void responseContains(String schema) {
		then().assertThat().body(matchesJsonSchemaInClasspath("schema/" + schema));
	}

	@Then("match image = \"(.+)\"")
	public void matchImageEquals(String expectedImage) throws IOException{
		logInfo("<pre>" + writer.toString() + "</pre>");
		
		String scenarioTestName = TestRule.getScenario().getName();
		
		byte[] resultImage = response.asByteArray();
		ByteArrayInputStream bis = new ByteArrayInputStream(resultImage);
		
        Iterator<?> readers = ImageIO.getImageReadersByFormatName("jpg");
        
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; 
        ImageInputStream iis = ImageIO.createImageInputStream(source); 
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
 
        Image image = reader.read(0, param);
 
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
 
        Graphics2D g2 = bufferedImage.createGraphics();
        g2.drawImage(image, null, null);
 
        File imageFile = new File("target/evidencias/html/img/" + scenarioTestName + "-response.jpg");
        ImageIO.write(bufferedImage, "jpg", imageFile);
        
		RestAssuredPage restAssuredPage = new RestAssuredPage();
		Assert.assertTrue("Returned image is different from expected", restAssuredPage.isImageEquals(expectedImage, scenarioTestName + "-response.jpg"));
	}
	

	/*
	 * Esta autenticacao do xviewer necessita ser explicada:
	 * 1 - Primeiro faz um POST /auroraservices-web/authentication/login para obter o sessionId
	 * 2 - Segundo faz um GET /auroraservices-web/authentication/jwtCreation para obter o hash jwt
	 * 3 - Terceiro faz um GET /auroraservices-web/hmac/hmacGenerator/?studyInstance=1.2.840.113619.1.1.1238255 para obter a queryString que deve ser usada no request seguinte ao Xviewer
	 * 4 - Quarto faz um POST /study/auth/?s1=1.2.840.113619.1.1.1238255&h=b2dab19188f5570b7d479e3f21ed2d59bca6e606&t=1591294681&r=0&cache=true para obter no header location o token berear
	 * 5 - Quinto e ultimo faz um GET /dcm/dicom_web_0/1.2.840.113619.1.1.1238255/1.2.840.113564.192168063.2020040722355181230/1.2.840.113564.192168063.2020040722355181240.1003000225002/0?wh=50,55&s=stf&pan=0,0&r=0&m=0&o=1&q=50 
	 * 	   com os ids do exame, serie e imagem passando como query string os parametros para manipular a imagem
	 */
	@Given("url to StudyInstanceUID \"(.*)\" SeriesInstanceUID \"(.*)\" SOPInstanceUID \"(.*)\"")
	public void jwtAuthentication(String studyInstanceUID, String seriesInstanceUID, String sopInstanceUID) throws MalformedURLException {
		urlByProp("pacs.server");
		path("auroraservices-web/authentication/login");
		headerParam("Content-Type", "application/json;charset=UTF-8");
		body("{\"login\":\"suporte.user\",\"hash\":\"" + Utils.getTestProperty("secret") + "\",\"password\":\"\",\"stationData\":{\"applicationCode\":\"070\",\"clientVersion\":\"1.0.0\",\"hardDrive\":\"\",\"ip\":\"\",\"macAddress\":\"\",\"monitor\":\"\",\"operationSystem\":\"Win32\",\"userMachine\":\"\"}}");
		post();
		String sessionId = response.getBody().jsonPath().getString("sessionId");
		
		urlByProp("pacs.server");
		path("auroraservices-web/authentication/jwtCreation");
		headerParam("sessionId", sessionId);
		get();
		String jwt = response.getHeader("X-Aurora-JWT");
		
		urlByProp("pacs.server");
		path("auroraservices-web/hmac/hmacGenerator");
		queryParam("studyInstance", studyInstanceUID);
		headerParam("sessionId", sessionId);
		headerParam("x-aurora-jwt", jwt);
		get();
		String[] responseHmacSplited = response.getBody().asString().split("&.=");
		String StudyInstanceInResponse = responseHmacSplited[0];
		String hashHmac = responseHmacSplited[1];
		String time = responseHmacSplited[2];
		
		urlByProp("url");
		path("study/auth/");
		queryParam("s1", StudyInstanceInResponse);
		queryParam("h", hashHmac);
		queryParam("t", time);
		queryParam("r", "0");
		queryParam("cache", "true");
		post();
		String bearerToken = response.getHeader("Location").split("#")[1];
		
		urlByProp("url");
		String dcmPath = "dcm/dicom_web_0/" + studyInstanceUID + "/" + seriesInstanceUID + "/" + sopInstanceUID + "/0";
		path(dcmPath);
		headerParam("Authorization", "Bearer " + bearerToken);
	}
	
}