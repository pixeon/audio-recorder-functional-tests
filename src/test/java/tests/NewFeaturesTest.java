package tests;
import org.junit.ClassRule;
import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import util.TestRule;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features", tags = {"@new-feature", "not @quarentena"},
	  glue = {""}, monochrome = true, dryRun = false, plugin = {"json:target/cucumber.json","rerun:target/rerun.txt"})
public class NewFeaturesTest {

	@ClassRule
	public static TestRule testRule = new TestRule();

}