package tests;

import org.junit.ClassRule;
import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import util.TestRule;

@RunWith(Cucumber.class)
@CucumberOptions(features = "@target/rerun2.txt", 
	  glue = {""}, monochrome = true, dryRun = false, strict = true, plugin = {"rerun:target/rerun3.txt"})
public class Tentativa3Test {

	@ClassRule
	public static TestRule testRule = new TestRule();

}