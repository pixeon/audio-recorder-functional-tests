package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

public class DataBaseStatement {

	DataBaseConnection connection = new DataBaseConnection();

	public void executeStatment(String database, String sql) {
		int affectedrows = 0;

		try (Connection conn = connection.connectionDB(database);
				PreparedStatement pstmt = conn.prepareStatement(sql)) {
			affectedrows = pstmt.executeUpdate();

		} catch (SQLException ex) {
			System.out.println("Falha ao conectar no banco de dados " + "\n" + ex.getMessage());
		}
		System.out.println("Linhas afetadas: " + affectedrows);
	}

	public ResultSet executeQuery(String database, String query) {
		try (Connection conn = connection.connectionDB(database);
				PreparedStatement pstmt = conn.prepareStatement(query)) {
			ResultSet rs = pstmt.executeQuery();
			return rs;
		} catch (SQLException ex) {
			System.out.println("Falha ao conectar no banco de dados " + "\n" + ex.getMessage());
			return null;
		}
	}

	public static ArrayList<String> getTracesMongoDb(FindIterable<org.bson.Document> iterDoc) {
		MongoCursor<org.bson.Document> it = iterDoc.iterator();
		ArrayList<String> registros = new ArrayList<String>();
		int i = 0;
		while (it.hasNext()) {
			registros.add(it.next().toJson());
			System.out.println("Event add" + registros.get(i));
			i++;
		}
		return registros;
	}

	public ArrayList<String> getTracesBetweenDatesMongoDb(String key, String[] value) throws ParseException {
		MongoDatabase db = connection.createConnectionMongoDb();

		Date from = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS\'Z\'").parse(value[0]);
		Date to = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS\'Z\'").parse(value[1]);

		MongoCollection<org.bson.Document> collection = db.getCollection("trace");
		FindIterable<org.bson.Document> iterDoc = collection
				.find(Filters.and(Filters.gte(key, from), Filters.lt(key, to)));

		return getTracesMongoDb(iterDoc);
	}

	public ArrayList<String> querySimpleValueMongoDb(String key, String value) throws ParseException {
		MongoDatabase db = connection.createConnectionMongoDb();

		MongoCollection<org.bson.Document> collection = db.getCollection("trace");
		FindIterable<org.bson.Document> iterDoc = collection.find(Filters.eq(key, value));

		return getTracesMongoDb(iterDoc);
	}

	public ArrayList<String> getListAllValuesMongoDb() throws ParseException {
		MongoDatabase db = connection.createConnectionMongoDb();

		MongoCollection<org.bson.Document> collection = db.getCollection("trace");
		FindIterable<org.bson.Document> iterDoc = null;

		iterDoc = collection.find();

		ArrayList<String> traceDocument = getTracesMongoDb(iterDoc);
		return traceDocument;
	}
	
	public void dropCollection(String collection) {
		MongoDatabase db = connection.createConnectionMongoDb();

		MongoCollection<org.bson.Document> col = db.getCollection(collection);
		col.drop();
	}

}
