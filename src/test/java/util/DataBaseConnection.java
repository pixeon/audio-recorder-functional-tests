package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

public class DataBaseConnection {

	private String postgres[] = { "jdbc:postgresql://localhost:5432/aurora", "postgres", "pixnet@2018" };
	private String sqlserver[] = { "jdbc:jtds:sqlserver://win-h490hff654q:1433;DatabaseName=aurora", "sa", "Vagrant42" };

	/**
	 * Connect to the database
	 * @param Data Base Management System (postgres, sqlserver)
	 *
	 * @return a Connection object
	 */
	public Connection connectionDB(String dataBase) throws SQLException {
		if (dataBase.equals("postgres")) {
			return DriverManager.getConnection(postgres[0], postgres[1], postgres[2]);
		} else if (dataBase.equals("sqlserver")) {
			return DriverManager.getConnection(sqlserver[0], sqlserver[1], sqlserver[2]);
		}
		return null;
	}
	
	public MongoDatabase createConnectionMongoDb() {
		@SuppressWarnings("resource")
		MongoClient mongo = new MongoClient(new MongoClientURI("mongodb://" + Utils.getTestProperty("traceability.mongo.host.port") + "/?maxIdleTimeMS=120000"));
		MongoDatabase db = mongo.getDatabase("trace");
		return db;
	}

}
