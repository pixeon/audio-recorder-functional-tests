package util;

import java.awt.HeadlessException;
import java.awt.MouseInfo;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.cert.CertificateException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

import javax.imageio.ImageIO;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.X509Certificate;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.POITextExtractor;
import org.apache.poi.extractor.ExtractorFactory;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.extractor.XSSFExcelExtractor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Finder;
import org.sikuli.script.Pattern;

import com.aventstack.extentreports.ExtentReports;

public class Utils {

	private static String ambientePropertiesPath;

	public static Properties loadTestProperties() throws FileNotFoundException, IOException {
		Properties prop = new Properties();
		FileInputStream input = new FileInputStream(ambientePropertiesPath);
		prop.load(input);
		input.close();
		return prop;
	}

	public static String getTestProperty(String propertyKey) {
		try {
			//verifica se a property foi passada pelo maven, senao pega do arquivo de properties:
			if (System.getProperty(propertyKey) != null) {
				return System.getProperty(propertyKey);
			}
			Properties prop = loadTestProperties();
			return prop.getProperty(propertyKey).trim();
		} catch (Exception e) {
			String error_msg = String.format("Nao foi possivel carregar a propriedade %s. Erro %s", propertyKey,
					e.getMessage());
			throw new IllegalStateException(error_msg);
		}
	}
	
	public static Set<Object> getAllTestProperties() {
		Properties prop = loadProperties(ambientePropertiesPath);
		Set<Object> keys = prop.keySet();
		return keys;
	}

	public static Properties loadProperties(String propertiesPath) {
		Properties prop = new Properties();
		try {
			FileInputStream input = new FileInputStream(propertiesPath);
			prop.load(input);
			input.close();
		} catch (Exception e) {
			throw new RuntimeException("Cannot load propertie file: " + propertiesPath);
		}
		return prop;
	}

	public static String getProperty(Properties prop, String propertyKey) {
		try {
			//verifica se a property foi passada pelo maven, senao pega do arquivo de properties:
			if (System.getProperty(propertyKey) != null) {
				return System.getProperty(propertyKey);
			}
			return prop.getProperty(propertyKey).trim();
		} catch (Exception e) {
			String error_msg = String.format("Nao foi possivel carregar a propriedade %s. Erro %s", propertyKey,
					e.getMessage());
			throw new IllegalStateException(error_msg);
		}
	}

	public static void selecionarAmbiente() {
		String ambiente = "";
		//verifica se a property foi passada pelo maven, senao pega do arquivo de properties:
		if (System.getProperty("test.ambiente") != null) {
			ambiente = System.getProperty("test.ambiente");
		} else {
			ambiente = getAmbienteDoArquivoInitProperties();
		}
		
		File ambientePropertie = new File("src/test/resources/" + ambiente + ".properties");
		if(ambientePropertie.exists()) { 
			ambientePropertiesPath = "src/test/resources/" + ambiente + ".properties";
		} else {
			System.err.println("Nenhum ambiente conhecido foi selecionado. Crie o arquivo com as properties necessarias: " + "[src/test/resources/" + ambiente + ".properties]");
		}
	}

	public static String getAmbienteDoArquivoInitProperties() {
		Properties selecionarAmbiente = loadProperties("src/test/resources/selecionar-ambiente.properties");
		String ambiente = getProperty(selecionarAmbiente, "test.ambiente");
		return ambiente;
	}
	
	public static void addAllTestPropertiesToExtentReport(ExtentReports extentReport) {
        Set<Object> keys = Utils.getAllTestProperties();
        for(Object k:keys){
        	String key = (String)k;
        	extentReport.setSystemInfo(key, Utils.getTestProperty(key));
        }
	}

	public static String[] getSubDirectoriesNames(String path) {
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			public boolean accept(File current, String name) {
				return new File(current, name).isDirectory();
			}
		});
		return directories;
	}

	public static void deleteAllFilesInFolder(String folderPath) {
		try {
			FileUtils.cleanDirectory(new File(folderPath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void deleteFileInFolder(String file) {
		String pasta = "";

		if (Utils.isWindows()) {
			pasta = System.getProperty("user.dir") + "\\";
		} else {
			pasta = System.getProperty("user.dir") + "/";
		}
		File folder = new File(pasta);
		for (File f : folder.listFiles()) {
			if (!f.isDirectory()) {
				if (f.getName().contains(file)) {
					f.delete();
					System.out.println("Deletou o arquivo" + f.getName());
				}
			}
		}
	}
	
	public static void deleteFileInFolder(String file, String path) {
		String pasta = "";
		
		if (Utils.isWindows()) {
			pasta = path;
		} else {
			pasta = path.replace("\\", "/");
		}
		File folder = new File(pasta);
		for (File f : folder.listFiles()) {
			if (!f.isDirectory()) {
				if(f.getName().contains(file)) {
					f.delete();
					System.out.println("Deletou o arquivo" + f.getName());
				}
			}
		}
	}
	
	public static void copyFile(String sourceFilePath, String destFilePath){
		File sourceFile = new File(sourceFilePath);
		File destFile = new File(destFilePath);
	    try {
			Files.copy(sourceFile.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void copyDirectory(String sourceFilePath, String destFilePath){
		File source = new File(sourceFilePath);
		File dest = new File(destFilePath);
		try {
		    FileUtils.copyDirectory(source, dest);
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}

	public static File[] waitForFileExistsInPath(String dir, int timeOutInSeconds) {
		File fl = new File(dir);
		File[] files = fl.listFiles(new FileFilter() {
			public boolean accept(File file) {
				return file.isFile();
			}
		});

		int contador = 0;
		boolean atingiuTimeout = false;
		while (files.length == 0 && !atingiuTimeout) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			contador++;
			atingiuTimeout = timeOutInSeconds == contador / 5;
			files = fl.listFiles(new FileFilter() {
				public boolean accept(File file) {
					return file.isFile();
				}
			});
		}
		if (atingiuTimeout) {
			System.err.println("Nao foi gerado arquivo no caminho - " + dir + " apos " + timeOutInSeconds + " segundos");
		}
		
		return files;
	}

	public static String pdfFileToString(File pdfFilePath) {
		try {
			PDDocument doc = PDDocument.load(pdfFilePath);
			String pdfString = new PDFTextStripper().getText(doc);
			doc.close();
			return pdfString;
		} catch (InvalidPasswordException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "Arquivo pdf nao encontrado";
	}
	
	public static BufferedImage pdfFileToImage(File pdfFilePath) {
		try {
			PDDocument doc = PDDocument.load(pdfFilePath);
			PDFRenderer renderer = new PDFRenderer(doc);
			BufferedImage image = renderer.renderImage(0,4);
			doc.close();
			return image;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Converte arquivos Excel, Word, PowerPoint e Visio para String.
	 */
	public static String msOfficeFileToString(File msOfficeFilePath) {
		try {
			FileInputStream fis = new FileInputStream(msOfficeFilePath);
			POIFSFileSystem fileSystem = new POIFSFileSystem(fis);
			POITextExtractor oleTextExtractor = ExtractorFactory.createExtractor(fileSystem);
			return oleTextExtractor.getText();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "An error occurred while converting Microsoft Office file to String.";
	}

	/**
	 * Converte arquivos Excel de extensao .xlsx para String.
	 */
	public static String excelXLSXToString(File msExcelFilePath) {
		try {
			FileInputStream fis = new FileInputStream(msExcelFilePath);
			XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
			XSSFExcelExtractor textExtrator = new XSSFExcelExtractor(myWorkBook);
			String strTexto = textExtrator.getText();
			textExtrator.close();
			return strTexto;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "An error occurred while converting Microsoft Office file to String.";
	}

	public static File lastFileModified(String dir) {
		File fl = new File(dir);
		File[] files = fl.listFiles(new FileFilter() {
			public boolean accept(File file) {
				return file.isFile();
			}
		});

		long lastMod = Long.MIN_VALUE;
		File choice = null;
		for (File file : files) {
			if (file.lastModified() > lastMod) {
				choice = file;
				lastMod = file.lastModified();
			}
		}
		return choice;
	}

	public static String getDateTime(String formato) {
		DateFormat dateFormat = new SimpleDateFormat(formato);
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public static String getDateTime(String formato, int intAcrescimoDias) {
		DateFormat dateFormat = new SimpleDateFormat(formato);
		Calendar dataAtual = Calendar.getInstance();
		dataAtual.add(Calendar.DAY_OF_MONTH, intAcrescimoDias);
		return dateFormat.format(dataAtual.getTime());
	}

	public static Select retornarCombo(WebElement nomeCombo) {
		return new Select(nomeCombo);
	}

	public static String removeStringFromString(String stringContent, String stringToRemove) {
		int registroPosicaoInicial = stringContent.indexOf(stringToRemove);
		int registroTamanho = stringToRemove.length();
		int arquivoTamanho = stringContent.length();
		stringContent = stringContent.substring(0, registroPosicaoInicial) + "|"
				+ stringContent.substring(registroPosicaoInicial + registroTamanho, arquivoTamanho);
		return stringContent;
	}
	
	public static void removeLineFromFile(String filePath, String lineToRemove){
        File inputFile = new File(filePath);
        File tempFile = new File("target/temp/temp-file.csv");
        BufferedReader reader = null;
        BufferedWriter writer = null;
		try {
			reader = new BufferedReader(new FileReader(inputFile));
			writer = new BufferedWriter(new FileWriter(tempFile));
	        String currentLine;
			while((currentLine = reader.readLine()) != null) {
			    String trimmedLine = currentLine.trim();
			    if(trimmedLine.equals(lineToRemove)) continue;
			    writer.write(currentLine + System.getProperty("line.separator"));
			}
			writer.close();
			reader.close(); 
			Files.move(tempFile.toPath(), inputFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String obterDataAtual() {
	    SimpleDateFormat sdfDataAtual = new SimpleDateFormat("dd/MM/yyyy");
	    Date now = new Date();
	    String strDate = sdfDataAtual.format(now);
	    return strDate;
	}
	
	public static String obterHoraAtual() {
	    SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;
	}
	
	public static String obterDataHoraAtual() {
	    SimpleDateFormat sdfDataAtual = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	    Date now = new Date();
	    String strDate = sdfDataAtual.format(now);
	    return strDate;
	}
	
	public static String readFileToString(String path, Charset encoding) {
		byte[] encoded = null;
		try {
			encoded = Files.readAllBytes(Paths.get(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new String(encoded, encoding);
	}
	
	public static List<List<String>> csvAsList(String path){
		List<List<String>> csv = new ArrayList<List<String>>();
		String csvAsString =  readFileToString(path, StandardCharsets.UTF_8);
		String csvLines[] = csvAsString.split("\\r?\\n");
		for (String csvLine : csvLines) {
			csv.add(new ArrayList<String>(Arrays.asList(csvLine.replaceAll("\"", "").split("\\s*;\\s*"))));
		}
		return csv;
	}
	
	public static void killProcess(String process) {
		try {
			String osName = System.getProperty("os.name").toLowerCase();
			boolean isWindows = osName.startsWith("windows");
			if (isWindows) {
				Runtime.getRuntime().exec("taskkill /F /IM " + process);
			} else {
				Runtime.getRuntime().exec("kill -9 " + process);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static boolean isWindows() {
		return System.getProperty("os.name").toLowerCase().startsWith("windows");
	}
	
	public static boolean waitForAFileExistsInPath(String dir, String file, int timeOutInSeconds) {
		File[] files = waitForFileExistsInPath(dir, timeOutInSeconds);
		for (File arquivo : files) {
			if (file.equals(arquivo.getName())) {
				return true;
			}
		}
		return false;
	}
	
	public static void getMouseCoordinates() {
		while (true) {
			try {
				TimeUnit.SECONDS.sleep(3);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			double mouseX = MouseInfo.getPointerInfo().getLocation().getX();
			double mouseY = MouseInfo.getPointerInfo().getLocation().getY();

			System.out.println("X:" + mouseX);
			System.out.println("Y:" + mouseY);
		}
	}
	
	public static boolean verificarProcessoEmExecucacao(String[] processos) {
		for (String processo : processos) {
			try {
				String line;
				Process p = Runtime.getRuntime().exec("tasklist");
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				while ((line = input.readLine()) != null) {
					if (line.contains(processo)) {
						System.out.println("Processo est� rordando >> " + processo);
						return true;
					}
				}
				input.close();
			} catch (Exception err) {
				err.printStackTrace();
			}
		}
		System.out.println("Processo n�o est� rordando >> " + processos);
		return false;
	}
	
	public static boolean verificarProcessoEmExecucacao(String processo) {
		try {
			String line;
			Process p = Runtime.getRuntime().exec("tasklist");
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				if (line.toLowerCase().contains(processo.toLowerCase())) {
					System.out.println("Processo esta rordando >> " + processo);
					System.out.println("Line >> " + line);
					return true;
				}
			}
			input.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
		System.out.println("Processo nao esta rordando >> " + processo);
		return false;
	}
	
	public static String getClipBoardValue(){
		String data = null;
		try {
			data = (String) Toolkit.getDefaultToolkit()
			        .getSystemClipboard().getData(DataFlavor.stringFlavor);
		} catch (HeadlessException | UnsupportedFlavorException | IOException e) {
			e.printStackTrace();
		} 
		return data;
	}
	
	public static String removerCaracteresEspeciais(String string) {
		string = string.replaceAll("[^a-zA-Z0-9]", "");
		return string;
	}

	/**
	 * Metodo para comparacao se duas imagens sao iguais. A imagem deve estar na pasta target do projeto, basta passar o nome da imagem. Ex: imagem.png
	 * � possivel tambem comparar imagens de um pdf. Para isso, pode ser utilizado o metodo savePdfAsImage() para converter o pdf para imagem, 
	 * e em seguida passar o nome da imagem salva como parametro.
	 * @param String imagem1, String imagem2.
	 * @return true or false
	 * @throws IOException
	 * @throws FindFailed
	 */
	public static boolean compareImages(String imagem1, String imagem2) throws IOException, FindFailed {
		String diretorio;
		if(Utils.isWindows()) {
			diretorio = System.getProperty("user.dir") + "\\target\\";
		} else {
			diretorio = System.getProperty("user.dir") + "/target/";
		}
		System.out.println(diretorio+imagem1);
		Finder img1 = new Finder(diretorio+imagem1);
		img1.find(new Pattern(diretorio+imagem2).similar(0.99F));
		
		if (img1.hasNext()) {
			System.out.println("Imagens Iguais");
			return true;
		}
		System.out.println("Imagens diferentes");
		return false;
	}
	
	/**
	 * Salvar um arquivo pdf como imagem jpeg.
	 * @param arquivoPdf
	 * Nome do arquivo localizado na pasta target do projeto
	 * @return String
	 * Retorna o nome do arquivo jpeg criado
	 * @throws IOException
	 */
	public static String savePdfAsImage(String arquivoPdf) throws IOException {
		String diretorio = System.getProperty("user.dir") + "/target/";
		File pdf = new File(diretorio + arquivoPdf);
		String arquivoJpeg = arquivoPdf.toLowerCase().replace(".pdf", ".jpeg");
		ImageIO.write(Utils.pdfFileToImage(pdf), "JPEG", new File(diretorio + arquivoJpeg));
		return arquivoJpeg;
	}
	
	public static String getPasswordRoot(String data) {
		String arquivoCSV = "senhas_root.csv";
		BufferedReader br = null;
		String linha = "";
		String csvDivisor = ";";
		String senhaRoot = "";

		try {
			br = new BufferedReader(new FileReader(arquivoCSV));
			while ((linha = br.readLine()) != null) {
				if (linha.contains(data)) {
					String[] senha = linha.split(csvDivisor);
					System.out.println("Data: " + senha[senha.length - 2]);
					System.out.println("Senha: " + senha[senha.length - 1]);
					senhaRoot = senha[senha.length - 1];
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return senhaRoot;
	}
	
	public static String execCmd(String cmd) {
		String val = "";
		try {
			Process proc = Runtime.getRuntime().exec(cmd);
			InputStream is = proc.getInputStream();
			@SuppressWarnings("resource")
			Scanner s = new Scanner(is);
			s.useDelimiter("\\A");
			if (s.hasNext()) {
				val = s.next();
			} else {
				val = "";
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return val;
	}
	
	public static Matcher substringRegex(String regex, String data) {
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(regex);
		Matcher matcher = pattern.matcher(data);
		matcher.find();
		return matcher;
	}

	public static String substringRegexGroup1(String regex, String data) {
		return substringRegex(regex, data).group(1);
	}

	public static void httpPost(String request, String parameters) {
		httpsIgnoreSecurityCertifacate();
		try {
			byte[] postData = parameters.getBytes(StandardCharsets.UTF_8);
			int postDataLength = postData.length;
			URL url = new URL(request);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("charset", "utf-8");
			conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			conn.setUseCaches(false);
			try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
				wr.write(postData);
			}
		} catch (Exception e) {
			throw new RuntimeException(request + " " + parameters + " " + e);
		}
		
	}

	private static void httpsIgnoreSecurityCertifacate() {
		/* Start of Fix */
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@SuppressWarnings("unused")
			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			@SuppressWarnings("unused")
			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}

			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws CertificateException {
				// TODO Auto-generated method stub
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws CertificateException {
				// TODO Auto-generated method stub
			}

		} };

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};
		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		/* End of the fix */
	}

	
}