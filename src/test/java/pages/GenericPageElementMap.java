package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class GenericPageElementMap extends BasePage {

	public WebElement retornarElementoPeloTextoInformado(String descricao) {
		WebElement element = driver.findElement(By.xpath("//div[@id='modalAbout']//*[contains(text(),'" + descricao + "')]"));
		return element;
	}
	
	WebElement botaoFechar = driver.findElement(By.xpath("//div[@id='modalAbout']//parent::button[@id='modal_about_close']"));

}
