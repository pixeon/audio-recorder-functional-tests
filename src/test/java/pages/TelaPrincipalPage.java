package pages;

import org.openqa.selenium.support.PageFactory;

import util.TestRule;
import util.Utils;

public class TelaPrincipalPage extends TelaPrincipalPageElementMap {

	public TelaPrincipalPage() {
		PageFactory.initElements(TestRule.getDriver(), this);
	}

	public void abrirGravador() {
		TestRule.openApplicationChrome(Utils.getTestProperty("url"));
	}

	public boolean apresentouCampoServidor() {
		try {
			waitElement(campoServidorTrebuchet, 10);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
