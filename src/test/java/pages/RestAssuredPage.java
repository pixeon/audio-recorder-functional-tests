package pages;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.sikuli.script.Finder;
import org.sikuli.script.ImagePath;

import com.aventstack.extentreports.MediaEntityBuilder;

import io.github.marcoslimaqa.sikulifactory.SikuliFactory;
import util.TestRule;

public class RestAssuredPage extends RestAssuredPageElementMap {

	public RestAssuredPage() {
		SikuliFactory.initElements(TestRule.getSikuli(), this);
	}

	public boolean isImageEquals(String expectedImage, String resultImagePath) throws IOException {
		Finder resultImage = new Finder(ImageIO.read(new File(System.getProperty("user.dir") + "/target/evidencias/html/img/" + resultImagePath)));
		
		FileUtils.copyFile(new File(ImagePath.find(System.getProperty("user.dir") + "/src/test/resources/sikuli-images/services/" + expectedImage).getPath()), 
				new File(System.getProperty("user.dir") + "/target/evidencias/html/img/" + expectedImage));
		
		extentTest.info("Expected image: ", MediaEntityBuilder.createScreenCaptureFromPath("img/" + expectedImage).build());
		extentTest.info("Result image: ", MediaEntityBuilder.createScreenCaptureFromPath("img/" + resultImagePath).build());
		resultImage.find(System.getProperty("user.dir") + "/src/test/resources/sikuli-images/services/" + expectedImage);
		
		if (resultImage.hasNext()) {
			Double comparisonScore = resultImage.next().getScore();
			Double expectedScore = 0.9999;

			boolean isImageEquals = comparisonScore > expectedScore;
			
			if (isImageEquals) {
				logPass("Completely equal images. Expected similarity: >" + expectedScore + " Result: " + comparisonScore);
				return true;
			} else {
				logFail("Images not equals. Expected similarity: >" + expectedScore + " Result: " + comparisonScore);
				return false;
			}
		}

		logFail("Completely different images");
		return false;
	}

}