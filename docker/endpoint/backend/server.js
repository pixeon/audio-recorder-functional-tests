const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload');
const multer  = require('multer') //use multer to upload blob data
const upload = multer(); // set multer to be the upload variable (just like express, see above ( include it, then use it/set it up))
const fs = require('fs');


// app.use(saudacao('Gustavo'))
app.use(express.static('assets/files'))

app.use(fileUpload())


app.use(bodyParser.urlencoded({extended: true}))

app.use(bodyParser.raw({type: 'audio/mpeg', limit: '100mb'}))

app.use(bodyParser.text())
app.use(bodyParser.json())


// app.use('/teste', (req, res, next) => {
//     res.send('Backend respondeu...')
//     next()
// })

// app.get('/clientes/relatorio', (req, res) => {
//     res.send(`Cliente relatório completo = ${
//         req.query.completo
//     } para o ano = ${
//         req.query.ano
//     }`)
// })

// app.get('/clientes/:id', (req, res) => {
//     res.send(`Cliente ${
//         req.params.id
//     } selecionado.`)
// })


//************MIDDLEWARE GRAVADOR*************


app.post('/:file', (req, res) => {
    const file = `${__dirname}/assets/files/${req.params.file}`;
    fs.writeFileSync(file, Buffer.from(new Uint8Array(req.files.file.data))); // write the blob to the server as a file
    console.log(`post do arquivo ${req.params.file}`)
    res.sendStatus(200);
})

app.put('/:file', (req, res) => {
    const file = `${__dirname}/assets/files/${req.params.file}`;
    fs.writeFileSync(file, Buffer.from(new Uint8Array(req.files.file.data))); // write the blob to the server as a file
    console.log(`put do arquivo ${req.params.file}`)
    res.sendStatus(200);
})

//************MIDDLEWARE GRAVADOR*************


app.listen(3300, () => {
    console.log("Executando server....")
})

